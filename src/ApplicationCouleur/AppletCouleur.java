package ApplicationCouleur;

import java.awt.GridLayout;

import javax.swing.JApplet;
 
public class AppletCouleur extends JApplet {

	@Override
	public void init() {
		this.setLayout(new GridLayout(1,1));
		this.add(new VueCouleur());
	}
}
