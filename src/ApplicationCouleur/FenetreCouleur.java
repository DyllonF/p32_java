package ApplicationCouleur;

import java.awt.GridLayout;

import javax.swing.JFrame;


public class FenetreCouleur extends JFrame {
	/**
	 * Construit une FenetreCouleur contenant un PanneauCouleur
	 */
	public FenetreCouleur() {
		// associer un titre � la FenetreCouleur
		this.setTitle("Conversions RVB");
		// selectionner le gestionnaire de mise en page de la FenetreCouleur
		this.setLayout(new GridLayout(1, 1));
		// creer une vue couleur et l'ajouter a la FenetreCouleur
		this.add(new VueCouleur());
		// afficher la FenetreCouleur
		this.pack();
		this.setVisible(true);
	}
}
