package tp2;

import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class VueJeton extends JPanel {

	public VueJeton() {
		controleurJeton Controleur=new controleurJeton();
		// TODO Auto-generated constructor stub
		this.setLayout(new GridLayout(2,2));
		JButton b1= new JButton("");
		b1.addActionListener(Controleur);
		JButton b2= new JButton("");
		b2.addActionListener(Controleur);
		JButton b3= new JButton("");
		b3.addActionListener(Controleur);
		JButton b4= new JButton("");
		b4.addActionListener(Controleur);
		b1.setIcon(new ImageIcon("\\git\\p32_java\\src\\tp2\\image.jpg"));
		this.add(b1);
		this.add(b2);
		this.add(b3);
		this.add(b4);
		}
	
	
	
	public static boolean estCaseVide(JButton cases) {
		return ((cases.getIcon() != null));

	}
	public static void deplacerJetton(JButton case1,JButton case2) {
		if(estCaseVide(case1)) {
			case2.setIcon(case1.getIcon());
			case1.setIcon(null);
		}
	}

}
