package tp2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
@SuppressWarnings("unused")
public class controleurJeton implements ActionListener{
	
	private enum etat{click1,click2};
	private etat etatcourant;
	private JButton but;
	
	public controleurJeton() {
		this.etatcourant= etat.click1;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JButton b= (JButton) e.getSource();
		switch (this.etatcourant) {
		case click1:
			if (VueJeton.estCaseVide(b)) {
				b.setEnabled(false);
				this.but=b;
				this.etatcourant=etat.click2;}
			break;
		case click2:
			this.but.setEnabled(true);
			VueJeton.deplacerJetton(but,b);
			this.etatcourant=etat.click1;
			break;
		default:
			break;
		}
	}

}
