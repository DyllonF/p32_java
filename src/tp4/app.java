package tp4;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class app {

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setLayout(new GridLayout(1, 1));
		f.add(new VueIHM());
		f.setTitle("Chessquito");
		f.setVisible(true);
		f.pack();
	}

}
