package tp4;

import java.awt.Color;
import java.awt.ItemSelectable;
import java.awt.MenuContainer;
import java.awt.image.ImageObserver;
import java.io.Serializable;

import javax.accessibility.Accessible;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class CaseIHM extends JButton
		implements ImageObserver, ItemSelectable, MenuContainer, Serializable, Accessible, SwingConstants {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int ligne;
	private int colonne;

	public CaseIHM(int ligne, int colonne, Color couleur) {
		super();
		this.ligne = ligne;
		this.colonne = colonne;
		this.setBackground(couleur);
	}
	
	public int getLigne() {
		return ligne;
	}

	public void setLigne(int ligne) {
		this.ligne = ligne;
	}

	public int getColonne() {
		return colonne;
	}

	public void setColonne(int colonne) {
		this.colonne = colonne;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
