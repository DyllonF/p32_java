package tp4;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.MenuContainer;
import java.awt.TextArea;
import java.awt.image.ImageObserver;
import java.io.Serializable;

import javax.accessibility.Accessible;
import javax.swing.JPanel;

public class VueIHM extends JPanel implements ImageObserver, MenuContainer, Serializable, Accessible {

	private static final long serialVersionUID = 1L;
	private CaseIHM[][] echequier;
	
	public VueIHM() {
		this.setLayout(new GridLayout(2, 1));

		JPanel center = new JPanel();
		this.add(center);
		center.setLayout(new GridLayout(4, 4));

		this.echequier = new CaseIHM[4][4];
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (i%2==0 && j%2==0 || i%2==1 && j%2==1) {
					this.echequier[i][j] = new CaseIHM(i,j,Color.GRAY);
				}else {
					this.echequier[i][j] = new CaseIHM(i,j,Color.white);
				}
				this.echequier[i][j].setPreferredSize(new Dimension(100, 100));
				center.add(this.echequier[i][j]);
			}
		}

		TextArea bot = new TextArea();
		this.add(bot);

	}
}
