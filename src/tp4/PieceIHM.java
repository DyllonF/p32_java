package tp4;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.Serializable;

import javax.accessibility.Accessible;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class PieceIHM extends ImageIcon implements Serializable, Accessible, Icon {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String couleurPiece;
	private String nomPiece;
	private String typeImage;
	
	
	public PieceIHM(String couleurPiece, String nomPiece, String typeImage) {
		this.couleurPiece = couleurPiece;
		this.nomPiece = nomPiece;
		this.typeImage = typeImage;
		
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	public String getCouleurPiece() {
		return couleurPiece;
	}


	public String getNomPiece() {
		return nomPiece;
	}


	public String getTypeImage() {
		return typeImage;
	}
	
	
	

}
