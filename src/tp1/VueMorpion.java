package tp1;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class VueMorpion extends JPanel{
	private JButton [] cases;
	private JLabel Joueur;
	private JLabel Resultat;
	
	public VueMorpion() {
		ControleurMorpion controleur = new ControleurMorpion(this);
		//All
		this.setLayout(new BorderLayout());
		JPanel p1 = new JPanel();
		this.add(p1,BorderLayout.NORTH);
		JPanel p2 = new JPanel();
		this.add(p2,BorderLayout.CENTER);
		this.Resultat = new JLabel(" ");
		this.add(this.Resultat,BorderLayout.SOUTH);
		
		//North
		p1.setLayout(new GridLayout(1,2,9,9));
		JButton effacer = new JButton("Effacer");
		p1.add(effacer);
		effacer.addActionListener(controleur);
		this.Joueur = new JLabel("Joueur 1");
		p1.add(this.Joueur);
		
		
		//Center
		p2.setLayout(new GridLayout(3,3, 1,1));
		this.cases =new JButton[9];
		for (int i=0 ; i<9;i++) {
			this.cases[i]=new JButton();
			this.cases[i].addActionListener(controleur);
			p2.add(this.cases[i]);
		}
	}
	
	public Couple coordonneesBtCaseGrille(JButton B) {
		for (int i=0;i<9;i++) {
			if(this.cases[i]==B) {
				return new Couple(i/3,i%3);
			}
		}
		return null;
	}
	
	public void initialiser() {
		for (JButton B : this.cases) {
			B.setText("");
			B.setEnabled(true);
		}
		this.Resultat.setText(" ");
		this.Joueur.setText("Joueur 1");
	}
	
	public void AfficherResultat(int numero) {
		this.Resultat.setText("Le joueur "+numero+" a win");
	}
	public void afficherJoueurCourant(int num) {
		this.Joueur.setText("Joueur"+num);
	}
}
