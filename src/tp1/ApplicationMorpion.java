package tp1;

import java.awt.GridLayout;

import javax.swing.JFrame;

public class ApplicationMorpion {
	public static void main(String[] arg) {
		JFrame f = new JFrame();
		f.setLayout(new GridLayout(1,1));
		f.add(new VueMorpion());
		f.setTitle("Morpion");
		f.setVisible(true);
		f.pack();
		f.setSize(300,300);
	}
}
