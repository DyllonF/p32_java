package tp1;


public class Couple {
	private int premier;
	private int second;
	
	public Couple(int premier, int second) {
		super();
		this.premier = premier;
		this.second = second;
	}

	public int getPremier() {
		return premier;
	}

	public int getSecond() {
		return second;
	}

}
