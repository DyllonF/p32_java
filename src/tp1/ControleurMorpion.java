package tp1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ControleurMorpion implements ActionListener{
	
	private enum Etat{Joueur1, Joueur2};
	private VueMorpion vue;
	private ModeleMorpion modele;
	private Etat etatCourant;
	private boolean partifini;
	
	public ControleurMorpion(VueMorpion vue) {
		this.vue = vue;
		this.modele= new ModeleMorpion();
		this.etatCourant= Etat.Joueur1;
		this.partifini=false;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JButton b= (JButton) e.getSource();
		if(b.getText().equals("Effacer")) {
			this.vue.initialiser();
			this.modele.initialiser();
			this.partifini=false;
			this.etatCourant=Etat.Joueur1;
		} else {
			if(!this.partifini) {
				switch(this.etatCourant) {
				case Joueur1 :
					b.setText("X");
					b.setEnabled(false);
					Couple c = this.vue.coordonneesBtCaseGrille(b);
					this.modele.setValeurCase(c.getPremier(),c.getSecond(),TypeCase.Joueur1);
					if(this.modele.casesAlignees(c.getPremier(),c.getSecond())) {
						this.vue.AfficherResultat(1);
						this.partifini=true;
					}else {
						this.vue.afficherJoueurCourant(2);
						this.etatCourant=Etat.Joueur2;
					}
					break;
				case Joueur2 :
					b.setText("O");
					b.setEnabled(false);
					c = this.vue.coordonneesBtCaseGrille(b);
					this.modele.setValeurCase(c.getPremier(),c.getSecond(),TypeCase.Joueur2);
					if(this.modele.casesAlignees(c.getPremier(),c.getSecond())) {
						this.vue.AfficherResultat(2);
						this.partifini=true;
					}else {
						this.vue.afficherJoueurCourant(2);
						this.etatCourant=Etat.Joueur1;
					}
					break;
				}
			}
		}
		
	}

}
