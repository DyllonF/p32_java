package Tp3;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ControleurMasterMind implements ActionListener {
	
	private VueMasterMind vue;
	private enum etat{ChoixCouleur,clickButtonPosition,ClickButtonValide};
	private etat etatcourant;
	private JButton but;
	private int Cpt;
	private ModeleMastermind m;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		JButton b= (JButton) e.getSource();
		switch (this.etatcourant) {
		case ChoixCouleur:
			if (this.vue.appartientPallette(b)) {
				this.but = b;
				this.etatcourant = etat.clickButtonPosition;
			}
			break;
		case clickButtonPosition:
			if (this.vue.appartientCombinaison(b, Cpt)) {
				b.setBackground(this.but.getBackground());
				System.out.println(this.etatcourant);
				if ((this.vue.combinaisonComplete(Cpt))) {
					this.etatcourant=etat.ClickButtonValide;
				} else {
					this.etatcourant=etat.ChoixCouleur;
				}
				System.out.println(this.etatcourant);
			}
			break;
		case ClickButtonValide:
			if(b.getText()=="Valider") {
				int [] tab = this.vue.combinaisonsEnEntier(Cpt);
				for (int i=0;i<4;i++) {
					System.out.println(tab[i]);
				}
				this.vue.afficherBp(Cpt, this.m.nbChiffresBienPlaces(this.vue.combinaisonsEnEntier(Cpt)));
				System.out.println( this.m.nbChiffresBienPlaces(this.vue.combinaisonsEnEntier(Cpt)));
				this.vue.afficherMp(Cpt, this.m.nbChiffresMalPlaces(this.vue.combinaisonsEnEntier(Cpt)));
				
				if (m.nbChiffresBienPlaces(this.vue.combinaisonsEnEntier(Cpt)) != 4) {
					this.vue.activerCombinaison(Cpt+1);
					this.vue.desactiverCombinaison(Cpt);
					this.etatcourant = etat.ChoixCouleur;
				}else {
					System.out.println("gagn�");
					int [] tamere = m.getCombinaison();
					this.vue.afficherCombinaisonsOrdinateur(tamere);
				}
			}
			this.Cpt++;
			break;
		}
			
	}

	public ControleurMasterMind(VueMasterMind vue,int taille,int nbcouleurs) {
		this.vue=vue;
		this.etatcourant = etat.ChoixCouleur;
		this.Cpt=0;
		this.m = new ModeleMastermind(taille, nbcouleurs);
		m.genererCombinaison();
	}

}
