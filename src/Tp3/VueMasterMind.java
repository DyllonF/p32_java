package Tp3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

public class VueMasterMind extends JPanel{
	
	private JButton combinaisonsJoueurIHM[][];
	private JTextField BpIHM[];
	private JTextField MpIHM[];
	private JTextField combinaisonOrdiIHM[];
	private int nbCouleurs;
	private int taille;
	static int NBMAX_COMBINAISONS;
	private JButton paletteIHM[];
	private ModeleMastermind m;
	
	
	
	public VueMasterMind(int taille,int nbcouleurs) {
		this.nbCouleurs=nbcouleurs;
		this.taille=taille;
		ControleurMasterMind controleur= new ControleurMasterMind(this,taille,nbcouleurs);
		
		this.setLayout(new BorderLayout());
		
		JPanel p1 = new JPanel();
		this.add(p1,BorderLayout.NORTH);
		p1.setLayout(new FlowLayout());
		p1.add(new JLabel("Couleurs : "));
		JPanel p11 = new JPanel();
		p1.add(p11);
		p11.setLayout(new GridLayout(1,6 ,2,2));
		this.paletteIHM=new JButton[10];
		for (int i= 0;i<10;i++){
			this.paletteIHM[i]=new JButton("");
			this.paletteIHM[i].addActionListener(controleur);
			p11.add(this.paletteIHM[i]);
			this.paletteIHM[i].setVisible(false);
			
		}
		for (int i =0; i<this.nbCouleurs;i++) {
			this.paletteIHM[i].setVisible(true);
		}
		this.paletteIHM[0].setBackground(Color.red);
		this.paletteIHM[1].setBackground(Color.orange);
		this.paletteIHM[2].setBackground(Color.yellow);
		this.paletteIHM[3].setBackground(Color.green);
		this.paletteIHM[4].setBackground(Color.blue);
		this.paletteIHM[5].setBackground(Color.cyan);
		this.paletteIHM[6].setBackground(Color.magenta);
		this.paletteIHM[7].setBackground(Color.pink);
		this.paletteIHM[8].setBackground(Color.black);
		this.paletteIHM[9].setBackground(Color.gray);
		
		
		//CENTER
		JPanel p2 = new JPanel();
		this.add(p2,BorderLayout.CENTER);
		p2.setLayout(new GridLayout(10,2));
		this.combinaisonsJoueurIHM=new JButton[this.getTaille()][10];
		this.BpIHM=new JTextField[10];
		this.MpIHM= new JTextField[10];
		for (int i =0; i<10;i++) {
			JPanel p21 = new JPanel();
			JPanel p22= new JPanel();
			//set Layout sous jpanel
			p21.setLayout(new GridLayout(1,4));
				for (int j = 0;j<this.getTaille();j++) {
					this.combinaisonsJoueurIHM[j][i]=new JButton();
					this.combinaisonsJoueurIHM[j][i].addActionListener(controleur);
					this.combinaisonsJoueurIHM[j][i].setEnabled(false);
					p21.add(this.combinaisonsJoueurIHM[j][i]);
					this.combinaisonsJoueurIHM[j][i].setBackground(null);
				}
			p22.setLayout(new GridLayout(2,2));
			p22.add(new JLabel("B"));
			p22.add(new JLabel("M"));
			this.BpIHM[i]=new JTextField();
			this.BpIHM[i].setEditable(false);
			this.MpIHM[i]=new JTextField();
			this.MpIHM[i].setEditable(false);
			p22.add(this.BpIHM[i]);
			p22.add(this.MpIHM[i]);
			p2.add(p21);
			p2.add(p22);
		}
		for (int i = 0;i<this.getTaille();i++) {
			this.combinaisonsJoueurIHM[i][0].setEnabled(true);
		}
		
		//South
		JPanel p3 = new JPanel();
		this.add(p3,BorderLayout.SOUTH);
		p3.setLayout(new GridLayout(2,2));
		p3.add(new JPanel());
		p3.add(new JPanel());
		JPanel p31 = new JPanel();
		p31.setLayout(new GridLayout(1,4));
		this.combinaisonOrdiIHM= new JTextField[4];
		for (int j = 0;j<4;j++) {
			this.combinaisonOrdiIHM[j]=new JTextField();
			this.combinaisonOrdiIHM[j].addActionListener(controleur);
			p31.add(this.combinaisonOrdiIHM[j]);
		}
		p3.add(p31);
		JButton butVal = new JButton("Valider");
		butVal.addActionListener(controleur);
		p3.add(butVal);
	}



	public int getNbCouleurs() {
		return nbCouleurs;
	}



	public int getTaille() {
		return taille;
	}
	
	public void activerCombinaison(int i) {
		for (int j=0;j<this.getTaille();j++) {
			this.combinaisonsJoueurIHM[j][i].setEnabled(true);
		}
	}
	
	public void desactiverCombinaison(int i) {
		for (int j=0;j<this.getTaille();j++) {
			this.combinaisonsJoueurIHM[j][i].setEnabled(false);
		}
	}
	
	public void afficherBp(int i,int nbBP) {
		this.BpIHM[i].setText(Integer.toString(nbBP));
	}
	
	public void afficherMp(int i,int nbMP) {
		this.MpIHM[i].setText(Integer.toString(nbMP));
	}
	
	public boolean appartientPallette(JButton b) {
		for (int i=0;i<this.getNbCouleurs();i++) {
			if ((b.getBackground()==this.paletteIHM[i].getBackground())) return true;
		}
		return false;
	}
	
	public static Color chiffreEnCouleur(int i) {
		switch (i) {
		case 0:
			return Color.red;
		case 1:
			return Color.orange;
		case 2:
			return Color.yellow;
		case 3:
			return Color.green;
		case 4:
			return Color.blue;
		case 5:
			return Color.cyan;
		case 6:
			return Color.magenta;
		case 7:
			return Color.pink;
		case 8:
			return Color.black;
		case 9:
			return Color.gray;
		}
		return null;
	}
	
	public static int couleurEnChiffre(Color c) {
		
		if (c==Color.red) return 0;
		else if(c==Color.orange) return 1;
		else if (c==Color.yellow) return 2;
		else if (c==Color.green) return 3;
		else if (c==Color.blue) return 4;
		else if (c==Color.cyan) return 5;
		else if (c==Color.magenta) return 6;
		else if (c==Color.pink) return 7;
		else if (c==Color.black) return 8;
		else if (c==Color.gray) return 9;
		return 10;
	}
	
	public boolean combinaisonComplete(int i) {
		for (int j=0;j<this.getTaille();j++) {
			if(!this.appartientPallette(this.combinaisonsJoueurIHM[j][i])) {
				return false;
			}
		}
		return true;
	}
	
	public int[] combinaisonsEnEntier(int i) {
			int [] tab = new int[this.getTaille()];
			for (int j=0;j<this.getTaille();j++) {
				tab[j]=couleurEnChiffre(this.combinaisonsJoueurIHM[j][i].getBackground());
			}
			return tab;
	}
	
	
	public void afficherCombinaisonsOrdinateur(int [] tabCouleurs) {
		for (int j=0;j<this.getTaille();j++) {
		}
	}
	
	public boolean appartientCombinaison(JButton b,int i) {
		for (int j=0;j<this.getTaille();j++) {
			if (b==this.combinaisonsJoueurIHM[j][i]) return true;
		}
		return false;
	}



	public ModeleMastermind getM() {
		return m;
	}
	
	
}
